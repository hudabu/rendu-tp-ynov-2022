## The project

The goal of this project is to create our own Git server on the Rocky-Linux environment and administer it as cleanly as possible.

To do this, we will use **Gitea** : After several searches, **Gitea** is the most suitable solution for this project, offering a simplified installation and a good optimization.

On the administration side, we will set up monitoring on the devices using **Discord** for notifications. **Backups** will be performed on another device dedicated to this task.

## Installation

The installations steps are separated into different documents.

⚠ In this project, all devices are Virtual Machines of Rocky-Linux in the same local network `192.168.10.0/24`.

### 1. Monitoring and Preparation (On every devices)

The first script you can use is the preparation script. This will prepare your devices for any services we use.

[Script here](scripts/preparation.sh)

Next, we install **Netdata** on all devices for monitoring using the following method :

I am using a personnal script to setup netdata. You can found it <a href="scripts/netdata_setup.sh">here</a>.

### 2. Gitea and Database

- <a href="DATABASE.md">Set up the remote database</a>.
- <a href="SERVER.md">Set up the gitea server</a>.

Optionnal :
- <a href="BACKUP.md">Set up the backup</a>.

### 3. Finish the installation

You can now go to 'your.ip.device:3000' on a browser to finish the installation !

You now have your Gitea working !

## Sources : 
- <a href="https://gitea.io/en-us/">Gitea</a> - *gitea.io*
  - <a href="https://docs.gitea.io/en-us/">Docs</a>
- <a href="https://guides.lib.berkeley.edu/how-to-write-good-documentation">Best Practices for Documenting Your Project</a> - *guides.lib.berkeley.edu*
- <a href="https://www.shellcheck.net/">Finds bugs in your shell scripts</a> - *www.shellcheck.net*
- <a href="https://www.tecmint.com/install-mysql-in-rocky-linux/">How to Install MySQL 8.0 on Rocky Linux and AlmaLinux</a> - *www.tecmint.com*
