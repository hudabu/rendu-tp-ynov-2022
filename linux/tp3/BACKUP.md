## 1. Creation of a NFS server

Here is the script I use to setup a NFS Server to host backups files :
[Go check here](scripts/dnfsServer_setup.sh)

Run it on your backup device.

It will ask you : 

- Your domain name
- The directory where you want to setup the NFS
- Your network IP

## 2. Configuration of NFS clients

The configurations of NFS client is include in my gitea server scripts.

##  3. Make a backup (Manually) :

Create a directory for the sim link
```bash
sudo mkdir /home/git/dump
```

Create the sim link
```bash
sudo -u git ln -s /usr/local/bin/gitea /home/git/dump/gitea
```

Create a session with git user
```bash
sudo su git 
```

Make the backup
```bash
/home/git/dump/gitea dump -c /etc/gitea/app.ini -w /home/git/dump
```

Move the file in the NFS directory
```bash
sudo mv /home/git/dump/gitea-dump-**********.zip /backups
```