#!/bin/bash
# Installation and setup of Mysql and creation of the database
# Jdev - 31/10/21

# 1. Installation of mysql:8.0
dnf install @mysql
systemctl start mysqld; systemctl enable mysqld

# 2. Enable Mysql working ports on firewall
firewall-cmd --add-port=3306/tcp --permanent
firewall-cmd --reload

# 3. Creation of the new DB and new User
# I re-use a script written by Saad Ismail - me@saadismail.net
# If /root/.my.cnf exists then it won't ask for root password
if [ -f /root/.my.cnf ]; then
	echo "Please enter the NAME of the new MySQL database! (example: database1)"
	read dbname
	echo "Please enter the MySQL database CHARACTER SET! (example: latin1, utf8, ...)"
	echo "Enter utf8 if you don't know what you are doing"
	read charset
	echo "Creating new MySQL database..."
	mysql -e "CREATE DATABASE ${dbname} /*\!40100 DEFAULT CHARACTER SET ${charset} */;"
	echo "Database successfully created!"
	echo "Showing existing databases..."
	mysql -e "show databases;"
	echo ""
	echo "Please enter the NAME of the new MySQL database user! (example: user1)"
	read username
	echo "Please enter the PASSWORD for the new MySQL database user!"
	echo "Note: password will be hidden when typing"
	read -s userpass
  echo "Please enter the user host!"
  read userhost
	echo "Creating new user..."
	mysql -e "CREATE USER ${username}@${userhost} IDENTIFIED BY '${userpass}';"
	echo "User successfully created!"
	echo ""
	echo "Granting ALL privileges on ${dbname} to ${username}!"
	mysql -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'${userhost}';"
	mysql -e "FLUSH PRIVILEGES;"
	echo "You're good now :)"
	exit
	
# If /root/.my.cnf doesn't exist then it'll ask for root password	
else
	echo "Please enter root user MySQL password!"
	echo "Note: password will be hidden when typing"
	read -s rootpasswd
	echo "Please enter the NAME of the new MySQL database! (example: database1)"
	read dbname
	echo "Please enter the MySQL database CHARACTER SET! (example: latin1, utf8, ...)"
	echo "Enter utf8 if you don't know what you are doing"
	read charset
	echo "Creating new MySQL database..."
	mysql -uroot -e "CREATE DATABASE ${dbname} /*\!40100 DEFAULT CHARACTER SET ${charset} */;"
	echo "Database successfully created!"
	echo "Showing existing databases..."
	mysql -uroot -e "show databases;"
	echo ""
	echo "Please enter the NAME of the new MySQL database user! (example: user1)"
	read username
	echo "Please enter the PASSWORD for the new MySQL database user!"
	echo "Note: password will be hidden when typing"
	read -s userpass
  echo "Please enter the user host!"
  read userhost
	echo "Creating new user..."
	mysql -uroot -e "CREATE USER ${username}@${userhost} IDENTIFIED BY '${userpass}';"
	echo "User successfully created!"
	echo ""
	echo "Granting ALL privileges on ${dbname} to ${username}!"
	mysql -uroot -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'${userhost}';"
	mysql -uroot -e "FLUSH PRIVILEGES;"
	echo "You're good now :)"
	exit
fi

# 4. Secure Mysql Installation
# Delete Remote connection for root
mysql -uroot -e DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
mysql -uroot -e FLUSH PRIVILEGES;