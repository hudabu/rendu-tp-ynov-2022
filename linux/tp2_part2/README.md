# TP2 pt. 2 : Maintien en condition opérationnelle
## I. Monitoring
### 1. Le concept
### 2. Setup

**🌞 Setup Netdata**

**🌞 Manipulation du service Netdata**
 
(Les opérations sont réalisées sur les deux machines)

- Déterminer s'il est actif, et s'il est paramétré pour démarrer au boot de la machine

```bash
[jdev@web ~]$ sudo systemctl status netdata
[...]
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 14:51:09 CEST; 4s ago
[...]
```

- Déterminer à l'aide d'une commande ss sur quel port Netdata écoute

```bash
[jdev@web ~]$ sudo ss -alnpt | grep netdata
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2860,fd=33))
LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2860,fd=5))
LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=2860,fd=32))
LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=2860,fd=6))
```

- Autoriser ce port dans le firewall

```bash
[jdev@web ~]$ sudo firewall-cmd --add-port=8125/tcp --permanent
success
[jdev@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[jdev@web ~]$ sudo firewall-cmd --reload
success
[jdev@web ~]$ sudo firewall-cmd --list-all | grep port
  ports: 80/tcp 8125/tcp 19999/tcp
[...]
```

**🌞 Setup Alerting**

<!> `/etc/netdata/health_alarm_notify.conf` de la doc = `/opt/netdata/etc/netdata/health_alarm_notify.conf` pour nous. <!>

- Ajustez la conf de Netdata pour mettre en place des alertes Discord

```bash
[jdev@web ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
[jdev@web ~]$ sudo systemctl restart netdata
```

Après test, les alertes arrivent bien sur mon Discord.

Je tape la commande `sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf` sur les machines.

**🌞 Config alerting**

(On skip cette partie pour le moment)

## II. Backup

**🖥️ VM `backup.tp2.linux`**

### 1. Intwo bwo
### 2. Partage NFS

**🌞 Setup environnement**

- Créer un dossier `/srv/backup/`. Il contiendra un sous-dossier pour chaque machine du parc, commencez donc par créer le dossier `/srv/backup/web.tp2.linux/` :

`[jdev@backup ~]$ sudo mkdir -p /srv/backup/web.tp2.linux/`

**🌞 Setup partage NFS**

- Je crois que vous commencez à connaître la chanson... Google "nfs server rocky linux" :

1. Configure NFS Server. : 
- `sudo dnf -y install nfs-utils`
- `sudo vi /etc/idmapd.conf`
```bash
[jdev@backup ~]$ sudo vi /etc/idmapd.conf
[jdev@backup ~]$ sudo cat /etc/idmapd.conf
[...]
Domain = tp2.linux
[...]
```
- `sudo vi /etc/exports`
```bash
[jdev@backup ~]$ sudo vi /etc/exports
[jdev@backup ~]$ sudo cat /etc/exports
# create new
# for example, set [/home/nfsshare] as NFS share
/srv/backup/web.tp2.linux 10.102.1.11/24(rw,no_root_squash)
```

- `sudo mkdir /home/nfsshare`
- `sudo systemctl enable --now rpcbind nfs-server`
```bash
[jdev@backup ~]$ sudo systemctl status rpcbind nfs-server
● rpcbind.service - RPC Bind
[...]
   Active: active (running) since Mon 2021-10-11 17:19:49 CEST; 7s ago
[...]

● nfs-server.service - NFS server and services
[...]
   Active: active (exited) since Mon 2021-10-11 17:19:49 CEST; 7s ago
[...]
```
2. Edit du `firewall-cmd` :

```bash
[jdev@backup ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
[jdev@backup ~]$ sudo firewall-cmd --reload
success
[jdev@backup ~]$ sudo firewall-cmd --list-all
public (active)
[...]
  services: nfs ssh
[...]
```

**🌞 Setup points de montage sur `web.tp2.linux`**

1. Configure NFS Client.

- `sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup/`
- Vérifier avec une commande mount que la partition est bien montée
```bash
[jdev@web ~]$ mount | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
```
- Vérifier avec une commande `df -h` qu'il reste de la place
```bash
[jdev@web ~]$ df -hT
[...]
backup.tp2.linux:/srv/backup/web.tp2.linux nfs4      6.2G  2.3G  4.0G  36% /srv/backup
[...]
```
- Vérifier avec une commande touch que vous avez le droit d'écrire dans cette partition
```bash
[jdev@web ~]$ sudo touch /srv/backup/touch_file
[jdev@web ~]$ sudo ls -al /srv/backup/
[...]
-rw-r--r--. 1 root root  0 Oct 12 00:36 touch_file
```

- Faites en sorte que cette partition se monte automatiquement grâce au fichier `/etc/fstab`

- `sudo vi /etc/fstab`
```bash
[jdev@web ~]$ sudo vi /etc/fstab
[jdev@web ~]$ sudo cat /etc/fstab
[...]
backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup/     nfs    defaults   0 0
```

**🌟 BONUS : partitionnement avec LVM**

- Je répère mon nouveau disque avec `lsblk` :
```bash
[jdev@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
[...]
sdb           8:16   0    8G  0 disk
[...]
```

### 3. Backup de fichiers

**Votre script DEVRA...**
- comporter un shebang
- comporter un commentaire en en-tête qui indique le but du script, en quelques mots
- comporter un commentaire qui indique l'auteur et la date d'écriture du script

**🌞 Rédiger le script de backup `/srv/tp2_backup.sh`**

**📁 Fichier <a href="assets/tp2_backup.sh">/srv/tp2_backup.sh</a>**

**🌞 Tester le bon fonctionnement**

- Exécuter le script sur le dossier de votre choix et prouvez que la backup s'est bien exécutée
```bash
[jdev@web ~]$ sudo /srv/tp2_backup.sh /srv/backup/ /etc/sysconfig/network-scripts
[...]
/etc/sysconfig/network-scripts/
/etc/sysconfig/network-scripts/ifcfg-enp0s3
/etc/sysconfig/network-scripts/ifcfg-enp0s8
[jdev@web ~]$ sudo ls -al /srv/backup
total 4
drwxr-xr-x. 2 root root  67 Oct 12  2021 .
drwxr-xr-x. 3 root root  67 Oct 12 03:09 ..
-rw-r--r--. 1 root root   0 Oct 12 00:36 touch_file
-rw-r--r--. 1 root root 426 Oct 12  2021 tp2_backup_21-10-12_03-09-41.tar.gz
```

- Tester de restaurer les données, récupérer l'archive générée, et vérifier son contenu

```bash
[jdev@web ~]$ mkdir test
[jdev@web ~]$ sudo tar -xf tp2_backup_21-10-12_03-09-41.tar.gz -C ~/test/
[jdev@web ~]$ sudo cat test/etc/sysconfig/network-scripts/ifcfg-enp0s8
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.102.1.11
NETMASK=255.255.255.0
```

**🌟 BONUS**

### 4. Unité de service
#### A. Unité de service

**🌞 Créer une unité de service pour notre backup**

- Créer un fichier texte dans le dossier `/etc/systemd/system/` avec le nom `tp2_backup.service`.
```bash
[jdev@web ~]$ sudo vim /etc/systemd/system/tp2_backup.service
[jdev@web ~]$ sudo cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own backup service (TP2_part2)

[Service]
ExecStart=/srv/tp2_backup.sh ~/test/ /etc/sysconfig/network-scripts
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

**🌞 Tester le bon fonctionnement**

- Exécuter `sudo systemctl daemon-reload` à chaque ajout/modification d'un service

On essaie de lancer le service :
```bash
[jdev@web ~]$ sudo systemctl start tp2_backup
Job for tp2_backup.service failed because the control process exited with error code.
See "systemctl status tp2_backup.service" and "journalctl -xe" for details.
[jdev@web ~]$ sudo systemctl status tp2_backup
● tp2_backup.service - Our own backup service (TP2_part2)
   Loaded: loaded (/etc/systemd/system/tp2_backup.service; disabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Tue 2021-10-12 07:36:55 CEST; 7s ago
[...]
Oct 12 07:36:55 web.tp2.linux tp2_backup.sh[10205]: rsync: mkdir "/~/test" failed: No such file or directory (2)
[...]
```
J'ai juste à fix ma route :
```bash
[jdev@web ~]$ sudo cat /etc/systemd/system/tp2_backup.service
[...]
ExecStart=/srv/tp2_backup.sh /test/ /etc/sysconfig/network-scripts
[...]
[jdev@web ~]$ sudo systemctl daemon-reload
[jdev@web ~]$ sudo systemctl start tp2_backup
[jdev@web ~]$ sudo systemctl status tp2_backup
● tp2_backup.service - Our own backup service (TP2_part2)
   Loaded: loaded (/etc/systemd/system/tp2_backup.service; disabled; vendor preset: disabled)
   Active: inactive (dead)

[...]
Oct 12 07:43:45 web.tp2.linux systemd[1]: tp2_backup.service: Succeeded.
Oct 12 07:43:45 web.tp2.linux systemd[1]: Started Our own backup service (TP2_part2).
```
- Prouvez que la backup s'est bien exécutée, vérifiez la présence de la nouvelle archive :
```bash
[jdev@web ~]$ ls -al /test
[...]
-rw-r--r--.  1 root root  426 Oct 12 07:43 tp2_backup_21-10-12_07-43-45.tar.gz
```

#### B. Timer

**🌞 Créer le timer associé à notre tp2_backup.service**

- Un fichier texte dans le dossier `/etc/systemd/system/` nommé `tp2_backup.timer`.
```bash
[jdev@web ~]$ sudo vim /etc/systemd/system/tp2_backup.timer
[jdev@web ~]$ sudo cat /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```
**🌞 Activez le timer**

- On deamon-reload : `sudo systemctl daemon-reload`
- Démarrer le timer : `sudo systemctl start tp2_backup.timer`
- Activer le au démarrage avec une autre commande systemctl : `sudo systemctl enable tp2_backup.timer`
- Prouver que le timer est actif actuellement et qu'il est paramétré pour être actif dès que le système boot :
```bash
[jdev@web ~]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Tue 2021-10-12 08:03:35 CEST; 10s ago
  Trigger: Tue 2021-10-12 08:04:00 CEST; 13s left

Oct 12 08:03:35 web.tp2.linux systemd[1]: Started Periodically run our TP2 backup script.
```

**🌞 Tests !**
```bash
[jdev@web ~]$ ls -al /test
[...]
-rw-r--r--.  1 root root  426 Oct 12 08:11 tp2_backup_21-10-12_08-11-01.tar.gz
[jdev@web ~]$ ls -al /test
[...]
-rw-r--r--.  1 root root  426 Oct 12 08:11 tp2_backup_21-10-12_08-11-01.tar.gz
-rw-r--r--.  1 root root  426 Oct 12 08:12 tp2_backup_21-10-12_08-12-01.tar.gz
```

<!> (Je le stop pour le moment car sans la règle des 5 backup max dans mon scirpt, je vais générer 1000 backups inutiles : `sudo systemctl stop tp2_backup.timer`)

#### C. Contexte

**🌞 Faites en sorte que...**

*Je remplace mon script par celui montré en cours pour avoir une base propre.*

- votre backup s'exécute sur la machine web.tp2.linux

- le dossier sauvegardé est celui qui contient le site NextCloud (quelque part dans /var/)

- la destination est le dossier NFS monté depuis le serveur backup.tp2.linux

```bash
[jdev@web ~]$ sudo cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own backup service (TP2_part2)

[Service]
ExecStart=/srv/tp2_backup.sh backup.tp2.linux:/srv/backup/web.tp2.linux /var/www/sub-domains/web.tp2.linux/html
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

- la sauvegarde s'exécute tous les jours à 03h15 du matin

[jdev@web ~]$ sudo cat /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* 03:15:00

[Install]
WantedBy=timers.target

- prouvez avec la commande sudo systemctl list-timers que votre service va bien s'exécuter la prochaine fois qu'il sera 03h15

**📁 Fichier /etc/systemd/system/tp2_backup.timer**</br>
**📁 Fichier /etc/systemd/system/tp2_backup.service**

### 5. Backup de base de données

**🌞 Création d'un script /srv/tp2_backup_db.sh**

**📁 Fichier `/srv/tp2_backup_db.sh`**

**🌞 Restauration**

**🌞 Unité de service**

**📁 Fichier /etc/systemd/system/tp2_backup_db.timer**</br>
**📁 Fichier /etc/systemd/system/tp2_backup_db.service**

### 6. Petit point sur la backup
## III. Reverse Proxy
### 1. Introooooo
### 2. Setup simple

**🖥️ VM `front.tp2.linux`**

**🌞 Installer NGINX**

- Vous devrez d'abord installer le paquet `epel-release` avant d'installer `nginx` : `sudo dnf -y install epel-release` -> `sudo dnf -y install nginx`

- Le fichier de conf principal de NGINX est `/etc/nginx/nginx.conf`.

**🌞 Tester !**

- Lancer le service nginx et le paramétrer pour qu'il démarre seul quand le système boot :
```bash
[jdev@front ~]$ sudo systemctl start nginx; sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[jdev@front ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-13 10:56:44 CEST; 10s ago
```
- Repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall :
```bash
[jdev@front ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      128          0.0.0.0:80         0.0.0.0:*    users:(("nginx",pid=27271,fd=8),("nginx",pid=27270,fd=8))
LISTEN 0      128             [::]:80            [::]:*    users:(("nginx",pid=27271,fd=9),("nginx",pid=27270,fd=9))
[jdev@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[jdev@front ~]$ sudo firewall-cmd --reload
success
[jdev@front ~]$ sudo firewall-cmd --list-all | grep ports
  ports: 8125/tcp 19999/tcp 80/tcp
[...]
```
- Vérifier que vous pouvez joindre NGINX avec une commande curl depuis votre PC
```bash
PS C:\Users\josep> curl 10.102.1.14:80                                                                                  

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...
RawContent        : HTTP/1.1 200 OK
[...]
```
**🌞 Explorer la conf par défaut de NGINX**

- Repérez l'utilisateur qu'utilise NGINX par défaut
```bash
[jdev@front ~]$ sudo cat /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
[...]
[jdev@front ~]$ ps -ef | grep nginx
root       27270       1  0 10:56 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      27271   27270  0 10:56 ?        00:00:00 nginx: worker process
jdev       27349    1419  0 11:11 pts/0    00:00:00 grep --color=auto nginx
```

L'utilisateur par défaut ici est `nginx`, mais root à lancé le process.
- Repérez le bloc server {} dans le fichier de conf principal :
```bash
[jdev@front ~]$ sudo vim /etc/nginx/nginx.conf
Shortcut vim : /server
[...]
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
[...]
```
- Par défaut, le fichier de conf principal inclut d'autres fichiers de conf, mettez en évidence ces lignes d'inclusion dans le fichier de conf principal

```bash
[jdev@front ~]$ sudo vim /etc/nginx/nginx.conf
Shortcut vim : /config
[...]
    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;
[...]
        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
[...]
```

**🌞 Modifier la conf de NGINX**

- Supprimer le bloc server {} par défaut, pour ne plus présenter la page d'accueil NGINX (Je fais une save du .config au cas où je l'explose)

```bash
[jdev@front ~]$ sudo cp /etc/nginx/nginx.conf /etc/nginx/nginxSaveConf.conf
[jdev@front ~]$ sudo vim /etc/nginx/nginx.conf
[jdev@front ~]$ sudo cat /etc/nginx/nginx.conf
[...]

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

# Settings for a TLS enabled server.
[...]
```

- Créer un fichier `/etc/nginx/conf.d/web.tp2.linux.conf` :
```bash
[jdev@front ~]$ sudo vim /etc/nginx/conf.d/web.tp2.linux.conf
[jdev@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
        # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
        listen 80;

        # ici, c'est le nom de domaine utilisé pour joindre l'application
        # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
        server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

        # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
        location / {
                # on renvoie tout le trafic vers la machine web.tp2.linux
                proxy_pass http://web.tp2.linux;
        }
}
[jdev@front ~]$ sudo systemctl restart nginx
[jdev@front ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-13 12:00:04 CEST; 4s ago
[...]
```
En tappant `10.102.1.14:80` depuis mon naviguateur j'arrive bien sur la page du NextCloud.

### 3. Bonus HTTPS

**🌟 Générer la clé et le certificat pour le chiffrement**

**🌟 Modifier la conf de NGINX**

**🌟 TEST**

## IV. Firewalling
### 1. Présentation de la syntaxe
### 2. Mise en place
#### A. Base de données

**🌞 Restreindre l'accès à la base de données `db.tp2.linux`**

```bash
[jdev@db ~]$ sudo firewall-cmd --list-all
[jdev@db ~]$ sudo firewall-cmd --set-default-zone=drop

[jdev@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent

[jdev@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/24 --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent

[jdev@db ~]$ sudo firewall-cmd --new-zone=db --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/24 --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent

[jdev@db ~]$ sudo firewall-cmd --reload
```

**🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```bash
[jdev@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/24
drop
  interfaces: enp0s3 enp0s8
ssh
  sources: 10.102.1.1/24
[jdev@db ~]$ sudo firewall-cmd --get-default-zone
drop
[jdev@db ~]$ sudo firewall-cmd --list-all --zone=drop; sudo firewall-cmd --list-all --zone=ssh; sudo firewall-cmd --list-all --zone=db
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/24
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
db (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/24
  services:
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### B. Serveur Web

**🌞 Restreindre l'accès au serveur Web `web.tp2.linux`**

```bash
[jdev@db ~]$ sudo firewall-cmd --list-all
[jdev@db ~]$ sudo firewall-cmd --set-default-zone=drop

[jdev@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent

[jdev@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/24 --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent

[jdev@db ~]$ sudo firewall-cmd --new-zone=rProxy --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=rProxy --add-source=10.102.1.14/24 --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=rProxy --add-port=80/tcp --permanent

[jdev@db ~]$ sudo firewall-cmd --reload
```

**🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```bash
[jdev@db ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
rProxy
  sources: 10.102.1.14/24
ssh
  sources: 10.102.1.1/24
[jdev@db ~]$ sudo firewall-cmd --get-default-zone
drop
[jdev@db ~]$ sudo firewall-cmd --list-all --zone=drop; sudo firewall-cmd --list-all --zone=ssh; sudo firewall-cmd --list-all --zone=rProxy
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/24
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
rProxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/24
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


#### C. Serveur de backup

**🌞 Restreindre l'accès au serveur de backup `backup.tp2.linux`**

```bash
[jdev@db ~]$ sudo firewall-cmd --list-all
[jdev@db ~]$ sudo firewall-cmd --set-default-zone=drop

[jdev@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent

[jdev@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/24 --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent

[jdev@db ~]$ sudo firewall-cmd --new-zone=backup --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=backup --add-service=nfs --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=backup --add-source=10.102.1.11/24 --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=backup --add-source=10.102.1.12/24 --permanent

[jdev@db ~]$ sudo firewall-cmd --reload
```

**🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```bash
[jdev@db ~]$ sudo firewall-cmd --get-active-zones
backup
  sources: 10.102.1.11/24 10.102.1.12/24
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/24
[jdev@db ~]$ sudo firewall-cmd --get-default-zone
drop
[jdev@db ~]$ sudo firewall-cmd --list-all --zone=drop; sudo firewall-cmd --list-all --zone=ssh; sudo firewall-cmd --list-all --zone=backup
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/24
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
backup (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/24 10.102.1.12/24
  services: nfs
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### D. Reverse Proxy

**🌞 Restreindre l'accès au reverse proxy `front.tp2.linux`**

```bash
[jdev@db ~]$ sudo firewall-cmd --list-all
[jdev@db ~]$ sudo firewall-cmd --set-default-zone=drop

[jdev@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent

[jdev@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/24 --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent

[jdev@db ~]$ sudo firewall-cmd --new-zone=clients --permanent
[jdev@db ~]$ sudo firewall-cmd --zone=clients --add-source=10.102.1.0/24 --permanent

[jdev@db ~]$ sudo firewall-cmd --reload
```

**🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```bash
[jdev@db ~]$ sudo firewall-cmd --get-active-zones
clients
  sources: 10.102.1.0/24
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/24
[jdev@db ~]$ sudo firewall-cmd --get-default-zone
drop
[jdev@db ~]$ sudo firewall-cmd --list-all --zone=drop; sudo firewall-cmd --list-all --zone=ssh; sudo firewall-cmd --list-all --zone=clients
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/24
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
clients (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## E. Tableau récap

**🌞 Rendez-moi le tableau suivant, correctement rempli :**

| Machine| IP| Service| Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`| `10.102.1.11` | Serveur Web| `22/tcp`; `80/tcp`| `10.102.1.1/24`; `10.102.1.14/24`|
| `db.tp2.linux`| `10.102.1.12` | Serveur Base de Données | `22/tcp`; `3306/tcp`| `10.102.1.1/24`; `10.102.1.11/24`|
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | `22/tcp`; `nfs`| `10.102.1.1/24`; `10.102.1.11/24`; `10.102.1.12/24`|
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy| `22/tcp` |`10.102.1.1/24`; `10.102.1.0/24`|

