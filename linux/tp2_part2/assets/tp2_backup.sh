#!/bin/bash
# Simple backup script
# Jdev - 10/10/2021

DIR_TO_BK=$2
DIR_WHERE_BK=$1

# Check if directory exist
if [[ -d $DIR_TO_BK ]]; then
  # Format date in $date
  # printf -v date '%(%Y-%m-%d_%H-%M-%S)T\n' -1
  FILENAME=$(date +tp2_backup_%y-%m-%d_%H-%M-%S.tar.gz)
  # printf -v name tp2_backup_$date.tar.gz
  #tar -czvf tp2_backup_$date.tar.gz $DIR_TO_BK
  tar -czvf $FILENAME $DIR_TO_BK
  rsync $FILENAME $DIR_WHERE_BK
else
  echo "No directory found. Must be a directory."
fi
