# TP4 : Vers un réseau d'entreprise

# 0. Prérequis
# I. Dumb switch
## 1. Topologie 1
## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

- Définissez les IPs statiques sur les deux VPCS

`ip 10.1.1.1 255.255.255.0` & `ip 10.1.1.2 255.255.255.0`

`ip dns 8.8.8.8`

- `ping` un VPCS depuis l'autre

```bash
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=14.540 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=10.445 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=12.061 ms
[...]
```

# II. VLAN

- déclaration du VLAN sur tous les switches
  - un VLAN a forcément un ID (un entier)
  - bonne pratique, on lui met un nom
- sur chaque switch, on définit le VLAN associé à chaque port
  - genre "sur le port 35, c'est un client du VLAN 20 qui est branché"

## 1. Topologie 2

## 2. Adressage topologie 2

| Node  | IP            | VLAN |Interface |
|-------|---------------|------|----------|
| `pc1` | `10.1.1.1/24` | 10   |Gi0/0     |
| `pc2` | `10.1.1.2/24` | 10   |Gi0/1     |
| `pc3` | `10.1.1.3/24` | 20   |Gi0/2     |

### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS

`ip 10.1.1.3 255.255.255.0`. Le reste est déjà défini.

- vérifiez avec des `ping` que tout le monde se ping

```bash
PC1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=8.164 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=6.375 ms
```
```bash
PC2> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=9.020 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=6.037 ms
```
```bash
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=14.624 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=11.589 ms
```

🌞 **Configuration des VLANs**

*Notabene : Pour annuler une commande Cisco : on rajoute `no` devant ex : `no vlan 10`*

- référez-vous [à la section VLAN du mémo Cisco](../../cours/memo/memo_cisco.md#8-vlan)
- déclaration des VLANs sur le switch `sw1`
- ajout des ports du switches dans le bon VLAN (voir [le tableau d'adressage de la topo 2 juste au dessus](#2-adressage-topologie-2))
  - ici, tous les ports sont en mode *access* : ils pointent vers des clients du réseau

Sur le switch :

```cmd
Switch>enable
Switch#conf t
Switch(config)#vlan 10
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 20
Switch(config-vlan)#name guests
Switch(config-vlan)#exit
Switch(config)#exit
Switch#sho
*Oct 21 13:30:04.112: %SYS-5-CONFIG_I: Configured from console by cons
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi0/1, Gi0/2, Gi0/3
                                                Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   admins                           active
20   guests                           active
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        0      0
10   enet  100010     1500  -      -      -        -    -        0      0
20   enet  100020     1500  -      -      -        -    -        0      0

```

```cmd
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
[...]
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
[...]
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
[...]
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
[...]
10   admins                           active    Gi0/0, Gi0/1
20   guests                           active    Gi0/2
```

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping

- PC1
```cmd
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=9.226 ms

PC1> ping 10.1.1.3

host (10.1.1.3) not reachable
```
- PC2
```cmd
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=11.407 ms

PC2> ping 10.1.1.3

host (10.1.1.3) not reachable
```

- `pc3` ne ping plus personne

```cmd
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```

# III. Routing

Dans cette partie, on va donner un peu de sens aux VLANs :

- un pour les serveurs du réseau
  - on simulera ça avec un p'tit serveur web
- un pour les admins du réseau
- un pour les autres random clients du réseau

## 1. Topologie 3

## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp4`, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***

🌞 **Configuration des VLANs**

- Déclaration des VLANs sur le switch `sw1` et ajout des ports du switches dans le bon VLAN :

```cmd
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit
Switch(config)#exit
Switch#show vlan br
[...]
11   clients                          active
12   admins                           active
13   servers                          active
[...]
Switch#conf t
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit
Switch(config)#interface Gi0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit
Switch(config)#exit
Switch#show vlan br
[...]
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi0/3
```
- il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* : c'est un port entre deux équipements réseau (un *switch* et un *routeur*)

```cmd
Switch(config)#interface Gi1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       1,11-13
```

🌞 **Config du *routeur***

- attribuez ses IPs au *routeur*
  - 3 sous-interfaces, chacune avec son IP et un VLAN associé

```cmd
R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0
R1(config-if)#no shut
*Mar  1 00:27:20.919: %LINK-3-UPDOWN: Interface FastEthernet0/0, changed state to up
*Mar  1 00:27:21.919: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
R1#sh ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
[...]
```

🌞 **Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau

```bash
# PC1 --------------------------
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=27.187 ms
# PC2 --------------------------
PC2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=22.514 ms
# adm1 --------------------------
adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=15.759 ms
# serv1 --------------------------
[jdev@web1 ~]ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=34.7ms
```

- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux
  - ajoutez une route par défaut sur les VPCS

```cmd
PC1> ip 10.1.1.1 255.255.255.0 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254
```
```cmd
PC2> ip 10.1.1.2 255.255.255.0 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254
```
```cmd
adm1> ip 10.2.2.1 255.255.255.0 10.2.2.254
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254
```

  - ajoutez une route par défaut sur la machine virtuelle

```bash
[jdev@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
[...]
GATEWAY=10.3.3.254
```

  - testez des `ping` entre les réseaux

```cmd
PC1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=43.868 ms

PC1> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=26.786 ms
```

# IV. NAT

## 1. Topologie 4

## 2. Adressage topologie 4

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

- branchez à `eth1` côté Cloud
- côté routeur, il faudra récupérer un IP en DHCP
```cmd
R1#conf t
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#exit
*Mar  1 01:00:22.271: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet1/0, changed state to up
R1(config)#exit
*Mar  1 01:00:25.171: %SYS-5-CONFIG_I: Configured from console by console
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
[...]
FastEthernet1/0            192.168.25.5    YES DHCP   up                    up
[...]
```
- vous devriez pouvoir `ping 1.1.1.1`
```cmd
R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 64/68/76 ms
```

🌞 **Configurez le NAT**

1. Repérage des interfaces internes / externes : 
```cmd
R1#show ip int br
```
*Internes*
```cmd
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
```
*Externes*
```cmd
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
```

2. Configurer les interfaces en "interne" ou "externe"
```cmd
R1(config)#interface fastethernet 0/0
R1(config-if)#ip nat inside
*Mar  1 01:15:58.827: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#exit
R1(config)#interface fastethernet 1/0
R1(config-if)#ip nat outside
R1(config-if)#exit
```

3. Définir une liste où tout le trafic est autorisé

`R1(config)#access-list 1 permit any`

4. Configurer le NAT

`R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload`

🌞 **Test**

- ajoutez une route par défaut (déjà fait)
  - sur les VPCS
  - sur la machine Linux
- configurez l'utilisation d'un DNS
  - sur les VPCS

```bash
# PC1 -------------
PC1> ip dns 8.8.8.8
# PC2 -------------
PC2> ip dns 8.8.8.8
# adm1 -------------
adm1> ip dns 8.8.8.8
```
  - sur la machine Linux

```bash
[jdev@web1 ~]$ sudo vi /etc/resolv.conf
[jdev@web1 ~]$ sudo cat /etc/resolv.conf
nameserver 8.8.8.8
```
- vérifiez un `ping` vers un nom de domaine
```cmd
PC1> ping google.com
google.com resolved to 216.58.215.46

84 bytes from 216.58.215.46 icmp_seq=1 ttl=113 time=40.012 ms
```
```cmd
PC2> ping google.com
google.com resolved to 142.250.178.142

84 bytes from 142.250.178.142 icmp_seq=1 ttl=113 time=33.836 ms
```
```cmd
adm1> ping google.com
google.com resolved to 216.58.215.46

84 bytes from 216.58.215.46 icmp_seq=1 ttl=113 time=39.803 ms
```
```bash
[jdev@web1 ~]$ ping google.com
PING google.com (216.58.204.110) 56(84) bytes of data.
64 bytes from par10s28-in-f14.1e100.net (216.58.204.110): icmp_seq=1 ttl=114 time=40.3 ms
```

# V. Add a building

## 1. Topologie 5

## 2. Adressage topologie 5

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node                | `clients`       | `admins`        | `servers`       |
|---------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`   | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`   | `10.1.1.2/24`   | x               | x               |
| `pc3.clients.tp4`   | DHCP            | x               | x               |
| `pc4.clients.tp4`   | DHCP            | x               | x               |
| `pc5.clients.tp4`   | DHCP            | x               | x               |
| `dhcp1.clients.tp4` | `10.1.1.253/24` | x               | x               |
| `adm1.admins.tp4`   | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4`  | x               | x               | `10.3.3.1/24`   |
| `r1`                | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

Switchs ports :

**sw1**

| Port of this Node | Connected to Node | On port |VLAN|
|-------------------|-------------------|---------|----|
| `Gi0/0` | `R1` | `FastEthernet0/0` |`trunk 11-13` |
| `Gi1/0` | `sw2` | `Gi1/0` |`trunk 11-13`|
| `Gi2/0` | `sw3` | `Gi2/0` |`trunk 11-13`|

**sw2**

| Port of this Node | Connected to Node | On port |VLAN|
|-------------------|-------------------|---------|----|
| `Gi0/0` | `PC1` | `Ethernet0` |`access 11`|
| `Gi0/1` | `PC2` | `Ethernet0` |`access 11`|
| `Gi0/2` | `adm1` | `Ethernet0` |`access 12`|
| `Gi0/3` | `web1` | `Ethernet0` |`access 13`|
| `Gi1/0` | `sw1` | `Gi1/0` |`trunk 11-13`|

**sw3**

| Port of this Node | Connected to Node | On port |VLAN|
|-------------------|-------------------|---------|----|
| `Gi0/0` | `PC3` | `Ethernet0` |`access 11`|
| `Gi0/1` | `PC4` | `Ethernet0` |`access 11`|
| `Gi0/2` | `PC5` | `Ethernet0` |`access 11`|
| `Gi0/3` | `dhcp1` | `Ethernet0` |`access 11`|
| `Gi2/0` | `sw1` | `Gi2/0` |`trunk 11-13`|

## 3. Setup topologie 5

Vous pouvez partir de la topologie 4. 

- [x] Définir l'ip de dhcp1
```bash
[jdev@dhcp1 ~]$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s3
[jdev@dhcp1 ~]$ sudo nmcli con reload; sudo nmcli con up enp0s3
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)
[jdev@dhcp1 ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc 
[...]
    inet 10.1.1.253/24 brd 10.1.1.255 scope global noprefixroute enp0s3
[...]
```
- [x] Créer les VLANs sur les switchs
  - [x] sw1
  - [x] sw2
  - [x] sw3 
```cmd
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit
Switch(config)#exit
Switch>show vlan br
[...]
11   clients                          active
12   admins                           active
13   servers                          active
[...]
```
- [x] **Ajouts des ports dans les VLANs**
- [x] sw1
```cmd
Switch(config)#interface Gi0/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11-13
Switch(config-if)#exit
Switch(config)#interface Gi1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11-13
Switch(config-if)#exit
Switch(config)#interface Gi2/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11-13
Switch(config-if)#exit
Switch(config)#exit
*Oct 21 19:41:16.111: %SYS-5-CONFIG_I: Configured from console by console
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi0/0       on               802.1q         trunking      1
Gi1/0       on               802.1q         trunking      1
Gi2/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi0/0       1-4094
Gi1/0       1-4094
Gi2/0       1-4094

Port        Vlans allowed and active in management domain
Gi0/0       1,11-13
Gi1/0       1,11-13
Gi2/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi0/0       1,11-13
Gi1/0       1,11-13
Gi2/0       1,11-13
```
- [x] sw2
```cmd
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit
Switch(config)#interface Gi0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit
Switch(config)#exit
Switch#show vlan br
[...]
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi0/3
[...]
Switch(config)#interface Gi1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11-13
Switch(config-if)#exit
Switch(config)#exit
*Oct 21 19:35:22.396: %SYS-5-CONFIG_I: Configured from console by console
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       1,11-13
```
- [x] sw3
```cmd
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi2/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11-13
Switch(config-if)#exit
Switch(config)#exit
*Oct 21 19:32:07.921: %SYS-5-CONFIG_I: Configured from console by consol
Switch#show vlan br
[...]
11   clients                          active    Gi0/0, Gi0/1, Gi0/2, Gi0/3
12   admins                           active
13   servers                          active
[...]
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi2/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi2/0       1-4094

Port        Vlans allowed and active in management domain
Gi2/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi2/0       1,11-13
```
- [x] **Config de `R1`**
```cmd
R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0
R1(config-if)#no shut
*Mar  1 00:05:06.299: %LINK-3-UPDOWN: Interface FastEthernet0/0, changed state to up
*Mar  1 00:05:07.299: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
R1(config-if)#exit
R1(config)#exit
*Mar  1 00:05:12.867: %SYS-5-CONFIG_I: Configured from console by console
R1#sh ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
[...]
```

- [x] **Setup des IPs statiques et de la Gateway**
- [x] PC1
```cmd
PC1> ip 10.1.1.1 255.255.255.0 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254
```

  - [x] PC2

```cmd
PC2> ip 10.1.1.2 255.255.255.0 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254
```

  - [x] adm1

```cmd
adm1> ip 10.2.2.1 255.255.255.0 10.2.2.254
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254
```
  - [x] web1
```cmd
[jdev@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
[...]
IPADDR=10.3.3.1
NETMASK=255.255.255.0
GATEWAY=10.3.3.254
```
  - [x] dhcp1
```cmd
[jdev@dhcp1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
[...]
IPADDR=10.1.1.253
NETMASK=255.255.255.0
GATEWAY=10.1.1.254
```

**ETA**

Mes machines peuvent se ping d'un réseau à l'autre :

```bash
# PC1 -----------
PC1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=46.755 ms
# PC2 -----------
PC2> ping 10.1.1.253

84 bytes from 10.1.1.253 icmp_seq=1 ttl=64 time=6.535 ms
```
- [x] **Setup du NAT**
```cmd
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#exit
R1(config)#exit
*Mar  1 00:29:15.571: %SYS-5-CONFIG_I: Configured from console by console
R1#
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
[...]
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
[...]
R1(config)#interface fastEthernet 0/0
R1(config-if)#ip nat inside
*Mar  1 00:32:40.047: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#exit
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip nat outside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fast
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload
```

- [x] Setup des DNS avec `ip dns 8.8.8.8`

- [x] Test d'un ping vers nom de domaine
```cmd
PC1> ping google.com
google.com resolved to 216.58.214.174

84 bytes from 216.58.214.174 icmp_seq=1 ttl=113 time=30.610 ms
```
```cmd
[jdev@web1 ~]$ ping google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=1 ttl=113 time=32.6 ms
```
```cmd
[jdev@dhcp1 ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=113 time=32.7 ms
```
🌞  **Vous devez me rendre le `show running-config` de tous les équipements**

- <a href="assets/sw1_config.txt">sw1# show running-config</a>
- <a href="assets/sw2_config.txt">sw2# show running-config</a>
- <a href="assets/sw3_config.txt">sw3# show running-config</a>
- <a href="assets/router_config.txt">R1# show running-config</a>

🖥️ **VM `dhcp1.client1.tp4`**

🌞  **Mettre en place un serveur DHCP dans le nouveau bâtiment**

1. Setup



- il doit distribuer des IPs aux clients dans le réseau `clients` qui sont branchés au même switch que lui
- sans aucune action manuelle, les clients doivent...
  - avoir une IP dans le réseau `clients`
  - avoir un accès au réseau `servers`
  - avoir un accès WAN
  - avoir de la résolution DNS

*Je clone le DHCP du tp3 et je modife l'ip, l'hostname etc ...*

```cmd
[jdev@dhcp1 ~] cat /etc/dhcp/dhcpd.conf
[...]
subnet 10.1.1.0 netmask 255.255.255.0 {
  [...]
  range 10.1.1.10 10.1.1.100;
  [...]
  option routers 10.1.1.254;
  option subnet-mask 255.255.255.0;
  [...]
  option domain-name-servers 8.8.8.8;
}
```

🌞  **Vérification**

- un client récupère une IP en DHCP
```cmd
PC3> ip dhcp
DDORA IP 10.1.1.10/24 GW 10.1.1.254
```
- il peut ping le serveur Web
```cmd
PC3> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=24.208 ms
```
- il peut ping `8.8.8.8`

```cmd
PC3> ping 8.8.8.8

8.8.8.8 icmp_seq=1 timeout
84 bytes from 8.8.8.8 icmp_seq=2 ttl=113 time=33.478 ms
```
- il peut ping `google.com`
```cmd
PC3> ping google.com
google.com resolved to 172.217.19.238

84 bytes from 172.217.19.238 icmp_seq=1 ttl=113 time=31.967 ms
```

*REX : J'ai vraiment aimé ce TP, on fini le réseau sur une bonne note de mon coté :D*
