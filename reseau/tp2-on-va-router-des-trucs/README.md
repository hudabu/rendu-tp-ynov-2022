
# TP2 : On va router des trucs
## 0. Prérequis
## I. ARP
### 1. Echange ARP
#### Générer des requêtes ARP
**Effectuer un `ping` d'une machine à l'autre :**

```console
[jdev@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.891 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.494 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.490 ms
--- 10.2.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2084ms
rtt min/avg/max/mdev = 0.490/0.625/0.891/0.188 ms
```
**observer les tables ARP des deux machines**

```console
[jdev@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:35 REACHABLE
10.2.1.12 dev enp0s8 lladdr 08:00:27:aa:f5:55 STALE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
```

```console
[jdev@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:db:67:7b STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:35 DELAY
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
```

**repérer l'adresse MAC de `node1` dans la table ARP de `node2` et vice-versa**

- Adresse MAC de `node2` dans la table ARP de `node1` : `... 08:00:27:aa:f5:55 ...`
- Adresse MAC de `node1` dans la table ARP de `node2` : `... 08:00:27:db:67:7b ...`

**prouvez que l'info est correcte**
- une commande pour voir la table ARP de `node1` :
	```console
	[jdev@node1 ~]$ ip n s
	10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:35 DELAY
	10.2.1.12 dev enp0s8 lladdr 08:00:27:aa:f5:55 STALE
	10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
	```
- et une commande pour voir la MAC de `node2` :
	```console
	[jdev@node1 ~]$ ip a
	[...]
	3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
	    link/ether 08:00:27:aa:f5:55 brd ff:ff:ff:ff:ff:ff
	[...]
	```
 ### 2. Analyse de trames
 **utilisez la commande `tcpdump` pour réaliser une capture de trame**
 
 **videz vos tables ARP, sur les deux machines, puis effectuez un `ping`**
 
 `ip -s -s neigh flush all`

**stoppez la capture, et exportez-la sur votre hôte pour visualiser avec Wireshark. Mettez en évidence les trames ARP :** 

<img src="assets/Wireshark_ARP.png" alt="Screen Wireshark des trames ARP"/>

**écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames**

| ordre | type trame  | source                      | destination                |
|-------|-------------|-----------------------------|----------------------------|
| 1     | Requête ARP | `node2` `08:00:27:aa:f5:55` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node1` `08:00:27:db:67:7b` | `node2` `08:00:27:aa:f5:55`|
| 3     | Requête ARP | `node1` `08:00:27:db:67:7b` | Broadcast `FF:FF:FF:FF:FF` |
| 4     | Réponse ARP | `node2` `08:00:27:aa:f5:55` | `node1` `08:00:27:db:67:7b`|

📁 <a href="assets/wireshark/tp2_arp.pcap">Capture réseau correspondante</a>
## II. Routage
### 1. Mise en place du routage
#### Activer le routage sur le noeud `router.net2.tp2`

On repère la zone utilisée par firewalld, généralement 'public' si vous n'avez pas fait de conf spécifique
`$ sudo firewall-cmd --list-all`
`$ sudo firewall-cmd --get-active-zone`
 
 Activation du masquerading
`$ sudo firewall-cmd --add-masquerade --zone=public`
`$ sudo firewall-cmd --add-masquerade --zone=public --permanent`

**Ajouter les routes statiques nécessaires pour que `node1.net1.tp2` et `marcel.net2.tp2` puissent se `ping`**

<u>Modification : </u>

`sudo nano /etc/sysconfig/network-scripts/route-enp0s8`

On ajoute les routes :
- `marcel` vers `router` : 
`10.2.1.0/24 via 10.2.2.254 dev enp0s8`
- `node1` vers `router` :
- `10.2.2.0/24 via 10.2.1.254 dev enp0s8`

<u>Vérification : </u>
Commande : `ip route show`
Sortie : 
```console
[jdev@node1 ~]$ ip route show
10.0.0.0/8 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
```
```console
[jdev@marcel ~]$ [jdev@marcel ~]$ ip route show
10.0.0.0/8 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
```
Commande depuis `marcel` : `ping 10.2.1.11`
Sortie :
```console
[jdev@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=0.969 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=0.910 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=0.985 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2027ms
rtt min/avg/max/mdev = 0.910/0.954/0.985/0.048 ms
```

### 2. Analyse de trames
#### Analyse des échanges ARP
**videz les tables ARP des trois noeuds**

Commande : `sudo ip neigh flush all`

**effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2**

Ping de `marcel` à `node1` :
 `[jdev@node1 ~]$ ping 10.2.2.12`

**regardez les tables ARP des trois noeuds et essayez de déduire un peu les échanges ARP qui ont eu lieu**

Commande : `ip neigh show`
Sorties combinées : 
```console
#marcel
10.2.2.254 dev enp0s8 lladdr 08:00:27:f1:c0:6f STALE
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:06 DELAY

#node1
10.2.1.254 dev enp0s8 lladdr 08:00:27:8c:48:89 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0c REACHABLE

#router
10.2.2.12 dev enp0s9 lladdr 08:00:27:56:a4:90 STALE
10.2.1.11 dev enp0s8 lladdr 08:00:27:db:67:7b STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0c DELAY
```
- `marcel` contacte `router`, `router` contacte `node1` 
- `node1` contacte `router`, `router` contacte `marcel`


node1 = `10.2.1.11` - `08:00:27:db:67:7b`
router = `10.2.1.254` - `08:00:27:8c:48:89` & `10.2.2.254` -  `08:00:27:f1:c0:6f` 
marcel = `10.2.2.12` - `08:00:27:56:a4:90`
node1 ping vers marcel
| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP |      `10.2.1.11`    | `node1` `08:00:27:db:67:7b`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `10.2.1.254`        | `router` `08:00:27:8c:48:89` | `10.2.1.11`              | `node1` `08:00:27:db:67:7b`   |
| 3   | Requête ARP    | `10.2.2.254`       | `router` `08:00:27:f1:c0:6f`                      |x                | Broadcast `FF:FF:FF:FF:FF`                |
| 4   | Réponse ARP | `10.2.2.12` |`marcel` `08:00:27:56:a4:90`|`10.2.2.254`| Router  `08:00:27:f1:c0:6f` |
| 5   | Requête ARP | `10.2.1.254` |`router` `08:00:27:8c:48:89` | `10.2.1.11` | Node1  `08:00:27:db:67:7b` |
| 6   | Réponse ARP | `10.2.1.11` |`node1` `08:00:27:db:67:7b` |`10.2.1.254`| Router  `08:00:27:8c:48:89` |
| 7   | Requête ARP | `10.2.2.12` | `marcel` `08:00:27:56:a4:90` |`10.2.2.254` | Router   `08:00:27:f1:c0:6f` |
| 8   | Réponse ARP | `10.2.2.254` | `router` `08:00:27:f1:c0:6f`  |`10.2.2.12`| Marcel  `08:00:27:56:a4:90` |
| 9     | Ping | `10.2.1.11` | `node1` `08:00:27:db:67:7b` |  `10.2.1.254` | `router`  `08:00:27:8c:48:89` |
| 10     | Pong        | `10.2.1.254` |`router`  `08:00:27:8c:48:89` |`10.2.1.11`| `node1` `08:00:27:db:67:7b`  |

📁 Capture réseau <a href="assets/wireshark/tp2_routage_node1.pcap">tp2_routage_node1.pcap</a>, <a href="assets/wireshark/tp2_routage_marcel.pcap">tp2_routage_marcel.pcap</a> et <a href="assets/wireshark/tp2_routage_router.pcap">tp2_routage_router.pcap</a>.
### 3. Accès internet
#### 🌞 Donnez un accès internet à vos machines
**ajoutez une route par défaut à `node1.net1.tp2` et `marcel.net2.tp2`**

Modification de fichier via `sudo nano /etc/sysconfig/network` : 
- Pour `node1` : `GATEWAY=10.2.1.254` 
- Pour `marcel` : `GATEWAY=10.2.2.254`

<u>Vérification :</u>

Avec `cat /etc/sysconfig/network` :

- Pour `node1` : 
```bash
# Created by anaconda
GATEWAY=10.2.1.254
```
- Pour `marcel` :
```bash
# Created by anaconda
GATEWAY=10.2.2.254
```

Je reboot ensuite les VMs.

**vérifiez que vous avez accès internet avec un `ping`**

Ping depuis `node1` :
```bash
[jdev@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=16.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=16.9 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 16.583/16.754/16.926/0.214 ms
```

Ping depuis `marcel` :
```bash
[jdev@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=15.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=16.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=16.3 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 15.917/16.291/16.638/0.294 ms
```

**donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser**

<u>Vérification :</u>

Avec `sudo cat /etc/resolv.conf`

- `node1` : 
```bash
# Generated by NetworkManager
nameserver 1.1.1.1
```
- `marcel` :
```bash
# Generated by NetworkManager
nameserver 1.1.1.1
```

**vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`**
```bash
[jdev@node1 ~]$ sudo dig gitlab.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31604
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             162     IN      A       172.65.251.78

;; Query time: 18 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Sep 27 10:29:29 CEST 2021
;; MSG SIZE  rcvd: 55
```

**puis avec un `ping` vers un nom de domaine**
```bash
[jdev@node1 ~]$ ping gitlab.com
PING gitlab.com (172.65.251.78) 56(84) bytes of data.
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=55 time=17.5 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=2 ttl=55 time=15.9 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=3 ttl=55 time=16.9 ms
^C
--- gitlab.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 15.867/16.751/17.473/0.665 ms
```
#### 🌞Analyse de trames

**effectuez un** `ping 8.8.8.8` **depuis** `node1.net1.tp2`

**analysez un ping aller et le retour qui correspond et mettez dans un tableau :**

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |     |
|-------|------------|---------------------|--------------------------|----------------|-----------------|-----|
| 1     | ping       | `node1` `10.2.1.12` | `node1` `08:00:27:db:67:7b` | `8.8.8.8`      | `router` `08:00:27:8c:48:89` |
| 2     | pong       | `8.8.8.8`| `router` `08:00:27:8c:48:89` | `10.2.1.11`|`node1` `08:00:27:db:67:7b` |

📁 <a href="assets/wireshark/tp2_routage_internet.pcap">Capture réseau correspondante</a>

## III. DHCP
### 1. Mise en place du serveur DHCP
#### 🌞**Sur la machine `node1.net1.tp2`, vous installerez et configurerez un serveur DHCP**
**installation du serveur sur `node1.net1.tp2`**
1. `dnf -y install dhcp-server`
2. `sudo nano /etc/dhcp/dhcpd.conf`
3. `systemctl enable --now dhcpd`
4. `sudo firewall-cmd --add-service=dhcp`
5. `sudo firewall-cmd --runtime-to-permanent`

#### 🌞**Améliorer la configuration du DHCP**

### 2. Analyse de trames
